package com.jams.nacosclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JamsNacosClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(JamsNacosClientApplication.class, args);
    }

}
